package tn.iit.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import tn.iit.entity.Student;
import tn.iit.exceptions.StudentNotFoundException;
import tn.iit.feignclients.AddressFeignClient;
import tn.iit.repository.StudentRepository;
import tn.iit.request.CreateAddressRequest;
import tn.iit.request.CreateStudentRequest;
import tn.iit.response.AddressResponse;
import tn.iit.response.StudentResponse;

@Service
public class StudentService {

	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	WebClient webClient;
	
	@Autowired
	AddressFeignClient addressFeignClient;

	public StudentResponse createStudent(CreateStudentRequest createStudentRequest) {

		Student student = new Student();
		student.setFirstName(createStudentRequest.getFirstName());
		student.setLastName(createStudentRequest.getLastName());
		student.setEmail(createStudentRequest.getEmail());
		
		CreateAddressRequest addressRequest = new CreateAddressRequest();
		addressRequest.setCity(createStudentRequest.getCity());
		addressRequest.setStreet(createStudentRequest.getStreet());
		
		AddressResponse addressResponse = addressFeignClient.createAddress(addressRequest);
		
		student.setAddressId(addressResponse.getAddressId());
		student = studentRepository.save(student);
		
		StudentResponse studentResponse = new StudentResponse(student, addressResponse);
		
		//studentResponse.setAddressResponse(getAddressById(student.getAddressId()));
		

		return studentResponse;
	}
	
	public StudentResponse getById (long id) {
		Optional<Student> studentOpt = studentRepository.findById(id);
		if(studentOpt.isPresent()) {
		Student student = studentRepository.findById(id).get();
		StudentResponse studentResponse = new StudentResponse(student, addressFeignClient.getById(student.getAddressId()));
		
		//studentResponse.setAddressResponse(getAddressById(student.getAddressId()));
		
		
		return studentResponse;
		}
		else
			throw new StudentNotFoundException("Etudiant non existant !!!");
	}
	
	public AddressResponse getAddressById (long addressId) {
		/*Mono<AddressResponse> addressResponse = 
				webClient.get().uri("/getById/" + addressId)
		.retrieve().bodyToMono(AddressResponse.class);
		
		return addressResponse.block();*/
		return addressFeignClient.getById(addressId);
	}
}
